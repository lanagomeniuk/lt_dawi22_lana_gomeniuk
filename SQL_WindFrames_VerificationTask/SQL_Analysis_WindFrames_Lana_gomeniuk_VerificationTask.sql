select calendar_year, calendar_quarter_desc, prod_category, sales as sales$,
	case calendar_quarter_number
			when 1 then 'N/A'
			else round((sales/quarter1 * 100)-100, 2)::text|| ' %' end as diff
from
(	
	select t.calendar_year, t.calendar_quarter_number,  t.calendar_quarter_desc, p.prod_category, sum(s.amount_sold) as sales,
		sum(sum(s.amount_sold)) over (
			partition by  t.calendar_year  order by  t.calendar_quarter_desc groups between unbounded preceding and current row) as cum_sum,
			first_value(sum(s.amount_sold)) over (
			partition by p.prod_category, t.calendar_year order by t.calendar_quarter_desc) as quarter1
	from products p
	inner join sales s on s.prod_id = p.prod_id
	inner join times t on t.time_id = s.time_id
	inner join channels c on c.channel_id = s.channel_id 
	where t.calendar_year in (1999, 2000) and
		p.prod_category in ('Electronics', 'Hardware', 'Software/Other')
		and c.channel_desc in ('Partners','Internet')
	group by t.calendar_year ,t.calendar_quarter_number, t.calendar_quarter_desc,  p.prod_category
	order by t.calendar_year , t.calendar_quarter_desc,  p.prod_category desc
) as tab