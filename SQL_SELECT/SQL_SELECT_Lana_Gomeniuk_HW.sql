-- PART 1

--All comedy movies released between 2000 and 2004, alphabetical

select f.title, f.release_year, c.name as genre from film f  
inner join film_category fc			-- I took film title, release year from film table and  category name from category table
on f.film_id = fc.film_id 			-- I joined film table and film_categoty table on film.id
inner join category c 		
on c.category_id = fc.category_id   -- then joined whit categoty table on category_id
where c.name = 'Comedy'
and f.release_year between '2000' and '2004';


--Revenue of every rental store for year 2017 
--(columns: address and address2 as one column, revenue)

select concat(address, ' ', address2) store_address, sum(p.amount) as revenue  from payment p
inner join staff s       	-- there is concat() to get adress and adress2 in one column, sum() function to get revenue
on s.staff_id = p.staff_id -- in this query were used 3 tables: payment, staff and store
inner join store s2 		-- payment was joined whit staff on staff_id
on s2.store_id  = s.store_id -- store whith adress on adress_id
inner join address a 
on a.address_id = s2.address_id
where p.payment_date between '2017-01-01' and '2017-12-31' 
group by store_address ;

--Top-3 actors by number of movies they took part in(columns:
--first_name, last_name, number_of_movies, sorted by number_of_movies in descending order)

select first_name, last_name, count(fa.film_id) as number_of_movies  from actor a
inner join film_actor fa 		-- there were joined actor and film_actor tables and used count() to count number of actor_id in film_actor table
on fa.actor_id = a.actor_id		-- there was used limit to get only 3 actors and desc - to get number_of_movies in descending order.
group by a.actor_id
order by number_of_movies desc 
limit 3;

--Number of comedy, horror and action movies per year (columns: release_year, number_of_action_movies, 
--number_of_horror_movies, number_of_comedy_movies), sorted by release year in descending order

select f.release_year , 
	sum(case when c."name" = 'Action' then 1 else 0 end ) as number_of_action_movies,
	sum(case when c."name" = 'Horror' then 1 else 0 end ) as number_of_horror_movies,
	sum(case when c."name" = 'Comedy' then 1 else 0 end) as  number_of_comedy_movies
from film f
inner join film_category fc 
on fc.film_id = f.film_id
inner join category c
on c.category_id = fc.category_id 
group by f.release_year
order by f.release_year desc ;


--select c.name, count(*) from category c
--inner join film_category fc  
--on c.category_id = fc.category_id
--where c.name in ('Comedy', 'Horror', 'Action')
--group by c.name ;


--PART 2

--Which staff members made the highest revenue for each store and deserve a bonus for 2017 year?

-- according to the result that I got after executing query, staff members whit staff_id 4 and 5 made highest revenue their stores.
with revenue as (select p.staff_id, concat(s.first_name, ' ', s.last_name) as staff_name,  sum(p.amount) as staff_revenue, s2.store_id  from  payment p 
	inner join staff s 
	on s.staff_id = p.staff_id 
	inner join store s2 
	on s2.store_id = s.store_id 
	where p.payment_date between '2017-01-01' and '2017-12-31'
	group by s2.store_id, p.staff_id, staff_name ),
store_revenue as (
	select  max(staff_revenue) as max_store_revenue, revenue.store_id
	from revenue 
	group by revenue.store_id)
select revenue.staff_id , revenue.staff_name, revenue.staff_revenue 
from revenue 
where revenue.staff_revenue in ( select max_store_revenue from store_revenue);


-- Which 5 movies were rented more than others and what's expected audience age for those movies?

-- according to the result 4 most popular movies are for families whit children, only one movie is not admitted for persons yanger than 18 years old. 
select f.title, f.rating, count(r.inventory_id) as rented_times from film f 
inner join inventory i 
on f.film_id = i.film_id 
inner join rental r 
on r.inventory_id  = i.inventory_id 
group by f.title, f.rating  
order by rented_times desc 
limit 5;


-- Which actors/actresses didn't act for a longer period of time than others?

-- According to mentor coments I've written the query which showes that RUSSEL BACALL was the actor that hasn't act since 2013

with max_year as ( 				-- I get  year that actor act last time
		select a.actor_id, concat(a.first_name, ' ', a.last_name) as actor_name, max(f.release_year) as last_act
		from actor a 
		inner join film_actor fa 
		on a.actor_id = fa.actor_id 
		inner join film f 
		on f.film_id = fa.film_id 
		group  by a.actor_id, actor_name 
		order by last_act),
	min_year as ( 					-- I get minimum value of years
		select min(last_act) as latest_act
		from max_year
		) 				-- I get actor who didn't act longer period by comparing actor's release yer of last movie whit value that I get in CTE min_year
select max_year.actor_id ,max_year.actor_name , max_year.last_act 
from max_year
where max_year.last_act = (select latest_act from min_year) ;


-- there is another query which result is the same as the first one has.
with last_act_year as (select a.actor_id, concat(a.first_name, ' ', a.last_name) as actor_name, max(f.release_year) as actor_last_act
	from actor a 
	inner join film_actor fa 
	on a.actor_id = fa.actor_id 
	inner join film f 
	on f.film_id = fa.film_id 
	group  by a.actor_id, actor_name 
	order by actor_last_act)
select * from last_act_year
where last_act_year.actor_last_act = (select min(actor_last_act) from last_act_year);
