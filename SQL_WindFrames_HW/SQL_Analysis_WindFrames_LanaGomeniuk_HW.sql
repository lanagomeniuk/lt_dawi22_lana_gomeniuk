------------------- TASK 1 ------------------

select country_region, calendar_year , amount_sold, by_channels|| ' %',
	   lag(by_channels , 3) over (order by country_region, calendar_year)|| ' %' as previous_period,
	   (by_channels - lag(by_channels , 3) over (order by country_region, calendar_year))|| ' %' as diff
from   
(
select  c.country_region, t.calendar_year, c3.channel_desc,
			sum(amount_sold) as amount_sold,
			sum(sum(amount_sold)) over region_year_rank as sum_year, 
			round(sum(amount_sold)*100/sum(sum(amount_sold)) over region_year_rank, 2)as by_channels
	from countries c
		inner join customers c2 on c2.country_id = c.country_id 
		inner join sales s on s.cust_id = c2.cust_id
		inner join channels c3 on c3.channel_id = s.channel_id 
		inner join times t on t.time_id = s.time_id
		where country_region in ('Americas', 'Asia', 'Europe') and
		  channel_desc in ('Direct Sales', 'Partners', 'Internet')
	group by c.country_region, t.calendar_year, c3.channel_desc
	window region_year_rank as (partition by c.country_region, t.calendar_year)
	order by c.country_region, t.calendar_year
) as tab
offset 3 rows;



--------------------- TASK 2 ---------------------------------------------
-- Build the query to generate a sales report for the 49th, 50th and 51st weeks of 1999. Add column CUM_SUM for accumulated amounts within 
-- weeks. For each day, display the average sales for the previous, current and next days (centered moving average, CENTERED_3_DAY_AVG column). 
-- For Monday, calculate average weekend sales + Monday + Tuesday. For Friday, calculate the average sales for Thursday + Friday + weekends.

select calendar_week_number, time_id, day_name, sales, cum_sum, round(CENTERED_3_DAY_AVG, 2) as CENTERED_3_DAY_AVG
from
(
	select  t.calendar_week_number , s.time_id, t.day_name, sum(s.amount_sold) as sales,
			sum(sum(s.amount_sold)) over (partition by t.calendar_week_number order by s.time_id)  as cum_sum,
		case 
			when t.day_name = 'Monday'
				then avg(sum(s.amount_sold)) over (order by s.time_id range between interval '2 day' preceding and '1 day' following)
			when t.day_name = 'Friday'
				then avg(sum(s.amount_sold)) over (order by s.time_id range between interval '1 day' preceding and '2 day' following)
			else avg(sum(s.amount_sold)) over (order by s.time_id range between interval '1 day' preceding and '1 day' following)
		end as  CENTERED_3_DAY_AVG
	from sales s 
			inner join times t on t.time_id = s.time_id
	where t.calendar_year = 1999 and t.calendar_week_number in (48, 49, 50, 51)
	group by t.calendar_week_number, s.time_id, t.day_name
	order by t.calendar_week_number 
) as tab
where calendar_week_number != 48;
	

--------------------- TASK 3 ---------------------------------------------

-- best sellenig prod_subcategory in product categories in America region;
select  prod_subcategory, prod_category, max_sales  from
(
select  p.prod_subcategory, p.prod_category, -- between unbounded preceding and  unbounded following is used to get best selling subcategory.
			sum(amount_sold) as amount_sold, -- frame start on the first value of each prod_category and the end is on the last value of the each prod_category 
			last_value(sum(amount_sold)) over (partition by p.prod_category order by sum(amount_sold) rows between unbounded preceding and  unbounded following) as max_sales
	from products p 
		inner join sales s on s.prod_id  = p.prod_id
		inner join customers c on c.cust_id = s.cust_id
		inner join countries c2 on c2.country_id = c.country_id
	where c2.country_region  = 'Americas'
	group by  p.prod_subcategory, p.prod_category
) as tabs 
where amount_sold = max_sales ;


-- the lowest sale in  'Monitors' subcategory
select prod_id, prod_name, amount_sold
from
(
	select p.prod_id, p.prod_name, sum(s.amount_sold) as amount_sold,  --, sum(sum(amount_sold))  over ( partition by p.prod_name order by sum(amount_sold)  groups between current row and unbounded following)
			first_value(sum(s.amount_sold)) over (order by sum(s.amount_sold) range between unbounded  preceding and unbounded following) as lowest_sale
	from products p 
		inner join sales s  on s.prod_id = s.prod_id
	where p.prod_subcategory ='Monitors'
	group by p.prod_id, p.prod_name, s.amount_sold
) as tab
where amount_sold = lowest_sale;

