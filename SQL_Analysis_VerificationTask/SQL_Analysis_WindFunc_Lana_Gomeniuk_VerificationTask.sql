-- 1. Build the query to generate a report about regions with the maximum number of products sold (quantity_sold)
-- for each channel for the entire period.

select channel_desc , country_region , sales , sales_percent
from 
(
	select  c.channel_desc, c3.country_region, 
			count(*) as sales,
			round((count(*) * 100 /  sum(count(*)) over (partition by c.channel_desc)), 2)|| ' %' as sales_percent,
			max(count(*)) over (partition by c.channel_desc order by count(*) desc) as max_sales
	from channels c 
		inner join sales s on s.channel_id = c.channel_id 
		inner join customers c2 on c2.cust_id = s.cust_id 
		inner join countries c3 on c3.country_id = c2.country_id
	group by c.channel_desc, c3.country_region
) as selection
where sales = max_sales;


-- 2. Define subcategories of products (prod_subcategory) for which sales for 1998-2001 have always been higher (sum(amount_sold))
-- compared to the previous year. The final dataset must include only one column (prod_subcategory).

select  prod_subcategory from 
(
	select t.calendar_year , p.prod_subcategory, sum(s.amount_sold) as sales,
			lag(sum(s.amount_sold) , 1, 0::numeric) over (partition by  p.prod_subcategory order by t.calendar_year) as previous_sales
	from products p 
		inner join sales s on s.prod_id = p.prod_id 
		inner join times t on t.time_id = s.time_id
	group by  p.prod_subcategory, t.calendar_year
) tab
where previous_sales < sales
group by prod_subcategory
having count(*) = 4;
