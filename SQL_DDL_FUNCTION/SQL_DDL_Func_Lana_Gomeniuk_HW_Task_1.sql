-- Create a function that will return the most popular film for each country (where country is an input paramenter).
-- The function should return the result set in the following view:
-- Query (example):select * from core.most_popular_films_by_countries(array['Afghanistan','Brazil','United States�]);

create or replace function public.most_popular_films_by_countries2(in countries_input text[])
returns table  (country_out text, film_out text, rating_out text, language_out text, length_out integer, release_year_out integer)
language plpgsql
as
$$
begin
	return query
	with total_rating as 
	(
		select c.country, f.title, f.rating, lang."name", f."length", f.release_year, count(r.rental_id) as total from country c
		inner join city c2
		on c2.country_id = c.country_id 
		inner join address ad
		on ad.city_id = c2.city_id 
		inner join customer cust
		on cust.address_id = ad.city_id 
		inner join rental r 
		on r.customer_id = cust.customer_id 
		inner join inventory i 
		on i.inventory_id = r.inventory_id 
		inner join film f 
		on f.film_id = i.film_id 
		inner join "language" lang
		on lang.language_id = f.language_id
		group by c.country, f.title, f.rating , lang."name" , f."length", f.release_year
	),
	max_rating as 
	(
		select total_rating.country, max(total) as max_film
		from total_rating 
		group by total_rating.country
	)
	select total_rating.country, total_rating.title, total_rating.rating::text, total_rating."name"::text, total_rating."length"::integer, total_rating.release_year::integer 
	from total_rating
	where total_rating.total in (select max_film from max_rating)
	and total_rating.country in (select unnest(countries_input));
end;
$$

select *from public.most_popular_films_by_countries2(array['Afghanistan','Brazil','United States'])
