-- Create function that inserts new movie with the given name in �film� table.
-- �release_year�, �language� are optional arguments. The function must return film_id of the inserted movie. 


create or replace function add_new_film(title_input text, language_input text)
returns setof bigint
language plpgsql
as $$
declare 
	lang_id int;
begin
	
-- lookup for language_id in "language" table
	select language_id  from "language" lang
	into lang_id 
	where upper(lang."name") = upper(language_input);

if not found then
	     raise notice 'Language "%" does not exists', language_input;
 
else 
	if title_input = (select title  from film where upper(title) = upper(title_input))
	then raise notice 'Film % already exists', title_input;
	else
-- insert new film into film table 
	insert into film (title, language_id)
	select title_input, lang_id
	where not exists (select * from film where upper(title) = upper(title_input)); -- checks whether film  already exists or not

-- select film_id of inserted film
	return query
	select film_id::bigint from film f 
	where upper(f.title)  = upper(title_input);
end if;
end if;
-- insert new film into film table 
end;
$$

select * from add_new_film('random film', 'english');