-- What operations do the following functions perform:
-- film_in_stock - this function returns film count in a particular store with selected film_id. it shows how many copies of film are in store 
 select * from public.film_in_stock(440, 2);

-- film_not_in_stock this function returns inventory_id of film that is not in stock at that moment in that particular store 
 select * from public.film_not_in_stock(440, 1);
 
-- inventory_in_stock - this function takes inventory_id as an input value and shows whether this copy of film is in stock
 select * from  public.inventory_in_stock(2026);
 
-- get_customer_balance
-- Why does �rewards_report� function return 0 rows? Correct and recreate the function, so that it's able to return rows properly.
----------------- no answer ------------

-- inventory_held_by_customer this function customer_id that film copy(inventory_id) is helded by
 select * from  public.inventory_held_by_customer(2026);

-- rewards_report
select * from  public.rewards_report(3, 3.00);

-- last_day this function takes timestamp as parametr and returns last day of month that is passed in the parametr
select * from last_day ('2005-12-27 19:40:33.000 +0300');
 
-- Why does �rewards_report� function return 0 rows? Correct and recreate the function, so that it's able to return rows properly.
----------------- no answer ------------

-- � Is there any function that can potentially be removed from the dvd_rental codebase? If so, which one and why?
-- I am not sure... maybe public.last_day(timestamp with time zone)

-- * How do �group_concat� and �_group_concat� functions work? (database creation script might help) Where are they used?
-- �_group_concat� is function that adds to strings together, �group_concat� is  a new aggregate function that combines information from different colunms and different rowes in one row

-- What does �last_updated� function do? Where is it used?
-- by this function is updated column last_update which is assigned to a CURRENT_TIMESTAMP

--  * What is tmpSQL variable for in �rewards_report� function? Can this function be recreated without EXECUTE statement and dynamic SQL? Why?
----------------- no answer ------------

