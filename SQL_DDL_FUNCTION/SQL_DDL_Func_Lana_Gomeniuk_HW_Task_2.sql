-- Create a function that will return a list of films by part of the title in stock (for example, films with the word 'love' in the title).
-- � So, the title of films consists of �%...%�, and if a film with the title is out of stock, please return a message: a movie with that title was not found
-- � The function should return the result set in the following view (notice: row_num field is generated counter field (1,2, �, 100, 101, �))


create or replace function get_films_in_stock_by_title (in title_exp text)
returns table (
	row_num bigint, film_title text, film_language text, customer_name text, film_rental_date date
)
language plpgsql
as $$
begin
	return query 
select ROW_NUMBER() OVER(ORDER BY f.title) AS row_num, f.title, l."name"::text, c.first_name, r.rental_date::date 
from film f
	inner join "language" l 
	on l.language_id = f.language_id 
	inner join inventory i 
	on i.film_id = f.film_id 
	inner join rental r 
	on r.inventory_id = i.inventory_id 
	inner join customer c 
	on c.customer_id = r.customer_id 
where f.title ilike title_exp;
if not found then
     raise notice'A movie with that title was not found' ;
  end if;
end;$$

select * from get_films_in_stock_by_title('%love%');