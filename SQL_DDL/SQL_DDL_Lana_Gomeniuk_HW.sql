create database kindergarden;

create schema kindergarden_data;


-- table child defined and created
create table if not exists kindergarden_data.child (
	child_id serial primary key,
	first_name text not null,
	last_name text not null,
	birth_date date not null,
	gender varchar(6),
	check(gender in ('Male', 'Female')) -- checks if gender is entered and it is one of two possilbe options
);


insert into kindergarden_data.child (first_name, last_name, birth_date, gender)
select 'Petras', 'Petraitis', '2018-01-02'::date, 'Male'
	where not exists (select * from child where upper(first_name)  = upper('Petras') and upper(last_name) = upper('Petraitis'))
union 
select 'Jonas', 'Jonas', '2018-06-12'::date, 'Male'
	where not exists (select * from child where upper(first_name)  = upper('Jonas') and upper(last_name) = upper('Jonas'))
union 
	select 'Ona', 'Onaite', '2018-08-25'::date, 'Female'
	where not exists (select * from child where upper(first_name)  = upper('Ona') and upper(last_name) = upper('Onaite'))
returning *;


-- table city defined and created
create table if not exists kindergarden_data.city (
	city_id serial primary key,
	city_name text not null
);

insert into kindergarden_data.city (city_name)
select 'Gargzdai'
	where not exists (select * from kindergarden_data.city where upper(city_name) = upper('Gargzdai'))
union
select 'Palanga'
	where not exists (select * from kindergarden_data.city where upper(city_name) = upper('Palanga'))
union
select 'Klaipeda'
	where not exists (select * from kindergarden_data.city where upper(city_name) = upper('Klaipeda'))
returning *;


-- table guardian defined and created
create table if not exists kindergarden_data.guardian (
	guardian_id serial primary key,
	first_name text not null,
	last_name text not null,
	phone_number text not null,
	email text not null,
	adress text not null,
	city_id integer references kindergarden_data.city,
	constraint chk_phone check (phone_number not like '%[^0-9]%'), -- phone number constraint checks is not a digit 
	constraint chk_email check (email like '%_@__%.__%')); -- checks if there are '@' and '.'


insert into kindergarden_data.guardian (first_name, last_name, phone_number, email, adress, city_id)
 select 'Anna', 'Jonaitiene', '867500006', 'jonaitiene@gmail.com', 'Laukinink? g. 7',
 		(select city_id from city where upper(city_name) = upper('Klaipeda'))
 		where not exists (select * from kindergarden_data.guardian where upper(email) = upper('jonaitiene@gmail.com'))
 union
 select 'Karolis', 'Petraitis', '867512456', 'petraitis@gmail.com', 'Laukinink? g. 37', 
 	(select city_id from city where upper(city_name) = upper('Klaipeda'))
 	where not exists (select * from kindergarden_data.guardian where upper(email) = upper('petraitis@gmail.com'))
union
select 'Lilija', 'Salverchuk', '860000001', 'salverchuk@gmail.com', 'Taikos pr. 144',
	(select city_id from city where upper(city_name) = upper('Klaipeda'))
 	where not exists (select * from kindergarden_data.guardian where upper(email) = upper('salverchuk@gmail.com'))
returning *;

 

-- table child_guardian defined and created
create table if not exists kindergarden_data.child_guardian (
	child_id integer references kindergarden_data.child not null,
	guardian_id integer references kindergarden_data.guardian not null
);
 
 
insert into kindergarden_data.child_guardian (child_id, guardian_id)
select  (select child_id from child where upper(first_name)  = upper('Petras') and upper(last_name) = upper('Petraitis')),
		(select guardian_id from kindergarden_data.guardian where upper(email) = upper('petraitis@gmail.com'))
		where not exists (select * from kindergarden_data.child_guardian  
							where guardian_id  = (select guardian_id from kindergarden_data.guardian where upper(email) = upper('petraitis@gmail.com')))
union 
select (select child_id from child where upper(first_name)  = upper('Jonas') and upper(last_name) = upper('Jonas')),
		(select guardian_id  from kindergarden_data.guardian where upper(email) = upper('jonaitiene@gmail.com'))
		where not exists (select * from kindergarden_data.child_guardian
							where guardian_id = (select guardian_id  from kindergarden_data.guardian where upper(email) = upper('jonaitiene@gmail.com')))
union 
select (select child_id  from child where upper(first_name)  = upper('Ona') and upper(last_name) = upper('Onaite')),
		(select guardian_id from kindergarden_data.guardian where upper(email) = upper('salverchuk@gmail.com'))
		where not exists (select * from kindergarden_data.child_guardian
							where guardian_id = (select guardian_id from kindergarden_data.guardian where upper(email) = upper('salverchuk@gmail.com')))
returning *;

-- table absence_reason defined and created
create table if not exists kindergarden_data.absence_reason (
	absence_reason_id serial primary key,
	absence_reason_description text not null
);


insert into kindergarden_data.absence_reason (absence_reason_description) 
select	'sick leave'
	where not exists (select * from kindergarden_data.absence_reason where upper(absence_reason_description) = upper('sick leave'))
union 
select 'parent has a vacation'
	where  not exists (select * from kindergarden_data.absence_reason where upper(absence_reason_description) = upper('parent has a vacation'))
union 
select 'day off'
	where  not exists (select * from kindergarden_data.absence_reason where upper(absence_reason_description) = upper('day off'))
returning *;

-- table attendance defined and created
create table if not exists kindergarden_data.attendance (
	attendance_id serial primary key,
	attendance_date date not null,
	child_id integer references kindergarden_data.child not null,
	absence_reason_id integer references kindergarden_data.absence_reason
);


insert into kindergarden_data.attendance (attendance_date, child_id)
select '2020-09-01'::date,
		(select child_id from child where upper(first_name)  = upper('Petras') and upper(last_name) = upper('Petraitis'))
		where not exists (select * from kindergarden_data.attendance where attendance_date = '2020-09-01'::date)
union
select '2020-09-01'::date, 
		(select child_id from child where upper(first_name)  = upper('Jonas') and upper(last_name) = upper('Jonas'))
		where not exists (select * from kindergarden_data.attendance where attendance_date = '2020-09-01'::date)
union 
select '2020-09-01'::date,
		(select child_id  from child where upper(first_name)  = upper('Ona') and upper(last_name) = upper('Onaite'))
		where not exists (select * from kindergarden_data.attendance where attendance_date = '2020-09-01'::date)
union
select '2020-09-02'::date,
		(select child_id  from kindergarden_data.child where upper(first_name)  = upper('Ona') and upper(last_name) = upper('Onaite'))
		where not exists (select * from kindergarden_data.attendance where attendance_date = '2020-09-02'::date)
returning *;


-- table classroom defined and created
create table if not exists kindergarden_data.classroom (
	classroom_id serial primary key,
	classroom_number integer not null,
	classroom_capacity integer not null
);

insert into kindergarden_data.classroom (classroom_number, classroom_capacity) 
select 1, 12
	where not exists (select * from kindergarden_data.classroom where classroom_number = 1)
union 
select 2, 15
	where not exists (select * from kindergarden_data.classroom where classroom_number = 2)
union 
select 3, 20
	where not exists (select * from kindergarden_data.classroom where classroom_number = 3)
returning *;


-- table group defined and created
create table if not exists kindergarden_data."group" (
	group_id serial primary key,
	group_title text not null,
	classroom_id integer references kindergarden_data.classroom not null
);

insert into kindergarden_data."group" (group_title, classroom_id)
select 'Cats', (select classroom_id from kindergarden_data.classroom where classroom_number = 1)
		where not exists (select * from kindergarden_data."group" where upper(group_title) = upper('Cats') )
union 
select 'Flowers', (select classroom_id from kindergarden_data.classroom where classroom_number = 2)
		where not exists (select * from kindergarden_data."group" where upper(group_title) = upper('Flowers'))
returning *;

-- table duty defined and created
create table if not exists kindergarden_data.duty (
	duty_id serial primary key,
	duty_title text not null,
	duty_description text not null
);


insert into kindergarden_data.duty (duty_title, duty_description)
select  'general teacher', ' includes planning, implementing and assessing lessons'
	where not exists (select * from kindergarden_data.duty where  upper(duty_title) = upper('general teacher'))
union
select 'PE teacher', ' PE teacher include teaching students how to play sports'
	where not exists (select * from kindergarden_data.duty where upper(duty_title) = upper('PE teacher'))
union 
select 'Music teacher', 'is responsible for teaching music'
	where not exists (select * from kindergarden_data.duty where upper(duty_title) = upper('Music teacher'))
returning *;


-- table teacher defined and created
create table if not exists kindergarden_data.teacher (
	teacher_id serial primary key,
	first_name text not null,
	last_name text not null,
	duty_id integer references kindergarden_data.duty not null
);


insert into kindergarden_data.teacher (first_name, last_name, duty_id)
select  'Lidija', 'Anochina',
		(select duty_id from kindergarden_data.duty where upper(duty_title) = upper('general teacher'))
	where not exists (select * from kindergarden_data.teacher where upper(first_name) = upper('Lidija') and upper(last_name) = upper('Anochina'))
union 
select 'Tatjana', 'Ivanova',
		(select duty_id from kindergarden_data.duty where upper(duty_title) = upper('general teacher'))
	where not exists (select * from kindergarden_data.teacher where upper(first_name) = upper('Tatjana') and upper(last_name) = upper('Ivanova'))
union 
select 'Katerina', 'Perec',
	(select duty_id from kindergarden_data.duty where upper(duty_title) = upper('Music teacher'))
	where not exists (select * from kindergarden_data.teacher where upper(first_name) = upper('Katerina') and upper(last_name) = upper('Perec'))
returning *;

-- table group_teacher defined and created

create table if not exists kindergarden_data.group_teacher (
	group_id integer references kindergarden_data."group" not null,
	teacher_id integer references kindergarden_data.teacher not null
);


insert into kindergarden_data.group_teacher (group_id, teacher_id)
select (select group_id from kindergarden_data."group" where upper(group_title) = upper('Cats')),
		(select teacher_id from kindergarden_data.teacher where  upper(first_name) = upper('Tatjana') and upper(last_name) = upper('Ivanova'))
where not exists (select * from kindergarden_data.group_teacher
					where group_id = (select group_id from kindergarden_data."group" where upper(group_title) = upper('Cats'))
					and teacher_id = ((select teacher_id from kindergarden_data.teacher where  upper(first_name) = upper('Tatjana') and upper(last_name) = upper('Ivanova'))))
union 
select (select group_id from kindergarden_data."group" where upper(group_title) = upper('Cats')),
		(select teacher_id from kindergarden_data.teacher where  upper(first_name) = upper('Lidija') and upper(last_name) = upper('Anochina'))
where not exists (select * from kindergarden_data.group_teacher
					where group_id = (select group_id from kindergarden_data."group" where upper(group_title) = upper('Cats'))
					and teacher_id = ((select teacher_id from kindergarden_data.teacher where  upper(first_name) = upper('Lidija') and upper(last_name) = upper('Anochina'))))
union
select (select group_id from kindergarden_data."group" where upper(group_title) = upper('Flowers')),
		(select teacher_id from kindergarden_data.teacher where  upper(first_name) = upper('Lidija') and upper(last_name) = upper('Anochina'))
where not exists (select * from kindergarden_data.group_teacher
					where group_id = (select group_id from kindergarden_data."group" where upper(group_title) = upper('Flowers'))
					and teacher_id = ((select teacher_id from kindergarden_data.teacher where  upper(first_name) = upper('Lidija') and upper(last_name) = upper('Anochina'))))
union
select (select group_id from kindergarden_data."group" where upper(group_title) = upper('Flowers')),
		(select teacher_id from kindergarden_data.teacher where  upper(first_name) = upper('Katerina') and upper(last_name) = upper('Perec'))
where not exists (select * from kindergarden_data.group_teacher
					where group_id = (select group_id from kindergarden_data."group" where upper(group_title) = upper('Flowers'))
					and teacher_id = ((select teacher_id from kindergarden_data.teacher where  upper(first_name) = upper('Katerina') and upper(last_name) = upper('Perec'))))
returning *;


-- table child_group defined and created
create table if not exists kindergarden_data.child_group (
	child_id integer references kindergarden_data.child not null,
	group_id integer references kindergarden_data."group" not null
);


insert into kindergarden_data.child_group (child_id, group_id)
select (select child_id from kindergarden_data.child where upper(first_name)  = upper('Petras') and upper(last_name) = upper('Petraitis')),
		(select group_id from kindergarden_data."group" where upper(group_title) = upper('Flowers'))
	where not exists  (select * from kindergarden_data.child_group
						where child_id = (select child_id from kindergarden_data.child where upper(first_name)  = upper('Petras') and upper(last_name) = upper('Petraitis'))
						and group_id = (select group_id from kindergarden_data."group" where upper(group_title) = upper('Flowers')))
union
select (select child_id from kindergarden_data.child where upper(first_name)  = upper('Petras') and upper(last_name) = upper('Petraitis')),
		(select group_id from kindergarden_data."group" where upper(group_title) = upper('Flowers'))
	where not exists (select * from kindergarden_data.child_group
						where child_id = (select child_id from kindergarden_data.child where upper(first_name)  = upper('Petras') and upper(last_name) = upper('Petraitis'))
						and group_id = (select group_id from kindergarden_data."group" where upper(group_title) = upper('Flowers')))
union
select (select child_id from child where upper(first_name)  = upper('Jonas') and upper(last_name) = upper('Jonas')),
		(select group_id from kindergarden_data."group" where upper(group_title) = upper('Flowers'))
	where not exists (select * from kindergarden_data.child_group
						where child_id = (select child_id from kindergarden_data.child where upper(first_name)  = upper('Jonas') and upper(last_name) = upper('Jonas'))
						and group_id = (select group_id from kindergarden_data."group" where upper(group_title) = upper('Flowers')))
union
select (select child_id  from child where upper(first_name)  = upper('Ona') and upper(last_name) = upper('Onaite')),
		(select group_id from kindergarden_data."group" where upper(group_title) = upper('Cats'))
	where not exists (select * from kindergarden_data.child_group
						where child_id = (select child_id from kindergarden_data.child where upper(first_name)  = upper('Ona') and upper(last_name) = upper('Onaite'))
						and group_id = (select group_id from kindergarden_data."group" where upper(group_title) = upper('Cats')))
returning *;


--  Alter all tables and add 'record_ts' field to each table.
-- Make it not null and set its default value to current_date.

alter table kindergarden_data.absence_reason add column record_ts date default current_date not null;
alter table kindergarden_data.attendance add column record_ts date default current_date not null;
alter table kindergarden_data.child add column record_ts date default current_date not null;
alter table kindergarden_data.child_group add column record_ts date default current_date not null;
alter table kindergarden_data.child_guardian add column record_ts date default current_date not null;
alter table kindergarden_data.city add column record_ts date default current_date not null;
alter table kindergarden_data.classroom add column record_ts date default current_date not null;
alter table kindergarden_data.duty add column record_ts date default current_date not null;
alter table kindergarden_data."group" add column record_ts date default current_date not null;
alter table kindergarden_data.group_teacher add column record_ts date default current_date not null;
alter table kindergarden_data.guardian add column record_ts date default current_date not null;
alter table kindergarden_data.teacher add column record_ts date default current_date not null;

