---- database created
create database medical_institutions;

---- schema created
create schema institution_data;

---- city tabble created
create table if not exists institution_data.city (
	city_id serial primary key,
	city_name text not null
);

insert into institution_data.city (city_name)
select 'Gargzdai' where not exists (select * from institution_data.city where upper(city_name) = upper('Gargzdai'))
union 
select 'Palanga' where not exists (select * from institution_data.city where upper(city_name) = upper('Palanga'))
union
select 'Klaipeda' where not exists (select * from institution_data.city where upper(city_name) = upper('Klaipeda'))
returning *;

---- person table created 
create table if not exists institution_data.person (
	person_id serial primary key,
	first_name text not null,
	last_name text not null,
	birth_date date not null,
	gender varchar(6),
	phone_number text not null,
	email text not null,
	adress text not null,
	city_id integer references institution_data.city,
	constraint chk_phone check (phone_number not like '%[^0-9]%'), -- phone number constraint checks is not a digit 
	constraint chk_email check (email like '%_@__%.__%'), -- checks if there are '@' and '.'
	constraint chk_gender check(gender in ('Male', 'Female')) -- checks if gender is entered and it is one of two possilbe options
);


-- -- person data inserted
with get_city as (
SELECT city_id FROM institution_data.city
where upper(city_name) = upper('Klaipeda')
)
insert into  institution_data.person (
	first_name, last_name,
	birth_date, gender,
	phone_number, email, adress, city_id
	)
select 'Anna', 'Jonaitiene', '1980-01-15'::date, 'Female', '867500006', 'jonaitiene@gmail.com', 'Laukininku g. 7-14', get_city.city_id
from get_city
	where not exists (select * from institution_data.person where email =  'jonaitiene@gmail.com')
union 
select 'Karolis', 'Petraitis', '1985-07-07'::date, 'Male', '867512456', 'petraitis@gmail.com', 'Mogiliovo g. 37-20', get_city.city_id
from get_city
	where not exists (select * from institution_data.person where email =  'petraitis@gmail.com')
union
select 'Lilija', 'Salverchuk', '1970-09-25'::date, 'Female', '860000001', 'salverchuk@gmail.com', 'Taikos pr. 144-28', get_city.city_id
from get_city
	where not exists (select * from institution_data.person where email =  'salverchuk@gmail.com')
union
select 'Sergej', 'Anochin', '1971-11-11'::date, 'Male', '861100045', 'anochin@gmail.com', 'Taikos pr. 103-32', get_city.city_id
from get_city
	where not exists (select * from institution_data.person where email =  'anochin@gmail.com')
union
select 'Lidija', 'Anochina', '1940-01-13'::date, 'Female', '861100229', 'lidija@gmail.com', 'Taikos pr. 103-32', get_city.city_id
from get_city
	where not exists (select * from institution_data.person where email =  'lidija@gmail.com')
	union
select 'Anton', 'Anochin', '2004-06-14'::date, 'Male', '860731635', 'anton@gmail.com', 'Taikos pr. 144-27', get_city.city_id
from get_city
	where not exists (select * from institution_data.person where email =  'anton@gmail.com')
returning *;

---- institution_type table created
---- Types of health care facilities

create table if not exists institution_data.institution_type (
	inst_type_id serial primary key,
	inst_type_name text not null,
	inst_type_description text not null
);

insert into institution_data.institution_type (inst_type_name, inst_type_description)
	select 'Hospital', 'Hospitals offer a wide range of medical, surgical and psychiatric services to their patients'
	where not exists (select * from institution_data.institution_type where inst_type_name =  'Hospital')
union
	select 'Clinic and medical office', 'Clinics and medical offices typically specialize in one or more areas of medicine and offer outpatient treatment that doesn�t require an overnight stay for patients'
	where not exists (select * from institution_data.institution_type where inst_type_name = 'Clinic and medical office')
union
	select 'Nursing home', 'Nursing homes are residential facilities that provide 24-hour care for the elderly or disabled.'
	where not exists (select * from institution_data.institution_type where inst_type_name = 'Nursing home')
union
	select 'Mental health center', 'Mental health treatment facilities offer psychiatric and psychological'
	where not exists (select * from institution_data.institution_type where inst_type_name = 'Mental health center')
union
	select 'Birth center', 'Birth centers are health care facilities that specialize in childbirth'
	where not exists (select * from institution_data.institution_type where inst_type_name = 'Birth center')
union
	select 'Hospice', 'Hospice care facilities aim to improve the quality of life for people with advanced and end-stage illnesses, as well as their families and caregivers'
	where not exists (select * from institution_data.institution_type where inst_type_name = 'Hospice')
returning *;


---- health_institution table created

create table if not exists institution_data.health_institution (
	health_inst_id serial primary key,
	health_inst_name text not null,
	inst_type_id integer references institution_data.institution_type,
	health_inst_adress text not null,
	city_id integer references institution_data.city
);

with get_hospital as (
	SELECT inst_type_id  FROM institution_data.institution_type 
	where upper(inst_type_name) = upper('Hospital')
),
get_clinic as (
	SELECT inst_type_id  FROM institution_data.institution_type 
	where upper(inst_type_name) = upper('Clinic and medical office')
)
insert into institution_data.health_institution (health_inst_name, inst_type_id, health_inst_adress, city_id)
select 'Republic Klaipeda Hospital', get_hospital.inst_type_id, ' S.N?ries st. 3',
	(select city_id from institution_data.city c where upper(c.city_name) = upper('Klaipeda'))
from get_hospital 
	where not exists (select * from institution_data.health_institution where upper(health_inst_name) = upper('Republic Klaipeda Hospital'))
union
select 'Klaipeda University Hospital', get_hospital.inst_type_id, 'Liepojos st. 41',
	(select city_id from institution_data.city c where upper(c.city_name) = upper('Klaipeda'))
from get_hospital 
	where not exists (select * from institution_data.health_institution where upper(health_inst_name) = upper('Klaipeda University Hospital'))
union
select 'Klaipeda Seamen�s Hospital', get_hospital.inst_type_id, 'Liepojos st. 45',
	(select city_id from institution_data.city c where upper(c.city_name) = upper('Klaipeda'))
from get_hospital 
	where not exists (select * from institution_data.health_institution where upper(health_inst_name) = upper('Klaipeda Seamen�s Hospital'))
union
select 'VNT medical office', get_clinic.inst_type_id, 'Taikos pr. 141',
	(select city_id from institution_data.city c where upper(c.city_name) = upper('Klaipeda'))
from get_clinic 
	where not exists (select * from institution_data.health_institution where upper(health_inst_name) = upper('VNT medical office'))
union
select 'Memel clinic', get_clinic.inst_type_id, 'Birutes st. 22',
	(select city_id from institution_data.city c where upper(c.city_name) = upper('Klaipeda'))
from get_clinic 
	where not exists (select * from institution_data.health_institution where upper(health_inst_name) = upper('Memel clinic'))
union
select 'NORTHWAY MEDICAL CENTER', get_clinic.inst_type_id, 'Dragunu st. 2',
	(select city_id from institution_data.city c where upper(c.city_name) = upper('Klaipeda'))
from get_clinic 
	where not exists (select * from institution_data.health_institution where upper(health_inst_name) = upper('NORTHWAY MEDICAL CENTER'))
	returning *;
	
---- doctor_type table created

create table if not exists institution_data.doctor_type (
	doc_type_id serial primary key,
	doc_type_name text not null
);
 

insert into institution_data.doctor_type (doc_type_name)
	select 'Cardiologist'
	where not exists (select * from institution_data.doctor_type where upper(doc_type_name) =  upper('Cardiologist'))
union
	select 'Dermatologist'
	where not exists (select * from institution_data.doctor_type where upper(doc_type_name) =  upper('Dermatologist'))
union
	select 'Endocrinologist'
	where not exists (select * from institution_data.doctor_type where upper(doc_type_name) =  upper('Endocrinologist'))
union
	select 'Family Physician'
	where not exists (select * from institution_data.doctor_type where upper(doc_type_name) =  upper('Family Physician'))
union
	select 'Gastroenterologist'
	where not exists (select * from institution_data.doctor_type where upper(doc_type_name) =  upper('Gastroenterologist'))
union 
	select 'Pulmonologist'
	where not exists (select * from institution_data.doctor_type where upper(doc_type_name) =  upper('Pulmonologist'))
returning *;


---- doctor_type table created

create table if not exists institution_data.medical_dtaff (
	staff_id serial primary key,
	first_name text not null,
	last_name text not null,
	license_number text not null unique,
	doc_type_id integer references institution_data.doctor_type,
	constraint chk_license check (license_number not like '%[^0-9]%')
);

insert into  institution_data.medical_dtaff(
	first_name, last_name,
	license_number, doc_type_id)
select 'Gintaras', 'Ivanauskas', '0012345',
	(select doc_type_id from institution_data.doctor_type where upper(doc_type_name) = upper('Gastroenterologist'))
where not exists (select * from institution_data.medical_dtaff where license_number =  '0012345')
union 
select 'Viktorija', 'Platonoviene', '2312367',
	(select doc_type_id from institution_data.doctor_type where upper(doc_type_name) = upper('Pulmonologist'))
where not exists (select * from institution_data.medical_dtaff where license_number =  '2312367')
union
select 'Aruna', 'Pangonis', '1123497',
	(select doc_type_id from institution_data.doctor_type where upper(doc_type_name) = upper('Endocrinologist'))
where not exists (select * from institution_data.medical_dtaff where license_number =  '1123497')
union
select 'Alevtina', 'Berezina', '0123400',
	(select doc_type_id from institution_data.doctor_type where upper(doc_type_name) = upper('Family Physician'))
where not exists (select * from institution_data.medical_dtaff where license_number =  '0123400')
union
select 'Taisija', 'Perec', '0123199',
	(select doc_type_id from institution_data.doctor_type where upper(doc_type_name) = upper('Family Physician'))
where not exists (select * from institution_data.medical_dtaff where license_number =  '0123199')
union
select 'Natalija', 'Korelina', '01231991',
	(select doc_type_id from institution_data.doctor_type where upper(doc_type_name) = upper('Family Physician'))
where not exists (select * from institution_data.medical_dtaff where license_number =  '01231991')
returning *;


---- institution_staff table created
create table if not exists institution_data.institution_staff (
	health_inst_id integer references institution_data.health_institution,
	staff_id integer references institution_data.medical_dtaff
);

insert into institution_data.institution_staff (health_inst_id, staff_id)
select (select health_inst_id from institution_data.health_institution where upper(health_inst_name) = upper('VNT medical office')),
		(select staff_id from institution_data.medical_dtaff where license_number =  '01231991')
union 
select (select health_inst_id from institution_data.health_institution where upper(health_inst_name) = upper('VNT medical office')),
		(select staff_id from institution_data.medical_dtaff where license_number =  '0123199')
union 
select (select health_inst_id from institution_data.health_institution where upper(health_inst_name) = upper('VNT medical office')),
		(select staff_id from institution_data.medical_dtaff where license_number =  '0123400')
union
select (select health_inst_id from institution_data.health_institution where upper(health_inst_name) = upper('Klaipeda University Hospital')),
		(select staff_id from institution_data.medical_dtaff where license_number =  '2312367')
union
select (select health_inst_id from institution_data.health_institution where upper(health_inst_name) = upper('Klaipeda University Hospital')),
		(select staff_id from institution_data.medical_dtaff where license_number =  '1123497')
union
select (select health_inst_id from institution_data.health_institution where upper(health_inst_name) = upper('Memel clinic')),
		(select staff_id from institution_data.medical_dtaff where license_number =  '1123497')
union
select (select health_inst_id from institution_data.health_institution where upper(health_inst_name) = upper('Republic Klaipeda Hospital')),
		(select staff_id from institution_data.medical_dtaff where license_number =  '0012345')
returning *;


---- appointment table created
create table if not exists institution_data.appointment(
	appointment_date date not null,
	health_inst_id integer references institution_data.health_institution,
	patient_id integer references institution_data.person,
	doctor_id integer references institution_data.medical_dtaff
);

insert into institution_data.appointment (appointment_date, health_inst_id, patient_id, doctor_id)
select '2022-01-05'::date,
		(select health_inst_id from institution_data.health_institution where upper(health_inst_name) = upper('VNT medical office')),
		(select person_id from institution_data.person where email =  'lidija@gmail.com'),
		(select staff_id from institution_data.medical_dtaff where license_number =  '0123199')
where not exists (select * from institution_data.appointment
					where patient_id = (select person_id from institution_data.person where email =  'lidija@gmail.com')
					and doctor_id = (select staff_id from institution_data.medical_dtaff where license_number =  '0123199'))
union 
select '2022-01-06'::date,
		(select health_inst_id from institution_data.health_institution where upper(health_inst_name) = upper('VNT medical office')),
		(select person_id from institution_data.person where email =  'anochin@gmail.com'),
		(select staff_id from institution_data.medical_dtaff where license_number =  '0123199')
where not exists (select * from institution_data.appointment
					where patient_id = (select person_id from institution_data.person where email =  'anochin@gmail.com')
					and doctor_id = (select staff_id from institution_data.medical_dtaff where license_number =  '0123199'))
union
select '2022-01-06'::date,
	(select health_inst_id from institution_data.health_institution where upper(health_inst_name) = upper('VNT medical office')),
	(select person_id from institution_data.person where email =  'anton@gmail.com'),
	(select staff_id from institution_data.medical_dtaff where license_number =  '0123400')
where not exists (select * from institution_data.appointment
					where patient_id = (select person_id from institution_data.person where email =  'anton@gmail.com')
					and doctor_id = (select staff_id from institution_data.medical_dtaff where license_number =  '0123400'))
union
select '2022-01-10'::date,
	(select health_inst_id from institution_data.health_institution where upper(health_inst_name) = upper('VNT medical office')),
	(select person_id from institution_data.person where email =  'salverchuk@gmail.com'),
	(select staff_id from institution_data.medical_dtaff where license_number =  '01231991')
where not exists (select * from institution_data.appointment
					where patient_id = (select person_id from institution_data.person where email =  'salverchuk@gmail.com')
					and doctor_id = (select staff_id from institution_data.medical_dtaff where license_number =  '01231991'))
union
select '2022-01-16'::date,
	(select health_inst_id from institution_data.health_institution where upper(health_inst_name) = upper('Memel clinic')),
	(select person_id from institution_data.person where email =  'salverchuk@gmail.com'),
	(select staff_id from institution_data.medical_dtaff where license_number =  '1123497')
where not exists (select * from institution_data.appointment
					where patient_id = (select person_id from institution_data.person where email =  'salverchuk@gmail.com')
					and doctor_id = (select staff_id from institution_data.medical_dtaff where license_number =  '1123497'))
union
select '2022-01-20'::date,
	(select health_inst_id from institution_data.health_institution where upper(health_inst_name) = upper('Memel clinic')),
	(select person_id from institution_data.person  where email =  'petraitis@gmail.com'),
	(select staff_id from institution_data.medical_dtaff where license_number =  '1123497')
where not exists (select * from institution_data.appointment
					where patient_id = (select person_id from institution_data.person where email =  'petraitis@gmail.com')
					and doctor_id = (select staff_id from institution_data.medical_dtaff where license_number =  '1123497'))
union
select '2022-01-20'::date,
	(select health_inst_id from institution_data.health_institution where upper(health_inst_name) = upper('Klaipeda University Hospital')),
	(select person_id from institution_data.person where email =  'lidija@gmail.com'),
	(select staff_id from institution_data.medical_dtaff where license_number =  '1123497')
where not exists (select * from institution_data.appointment
					where patient_id = (select person_id from institution_data.person where email =  'lidija@gmail.com')
					and doctor_id = (select staff_id from institution_data.medical_dtaff where license_number =  '1123497'))
union
select '2022-01-29'::date,
	(select health_inst_id from institution_data.health_institution where upper(health_inst_name) = upper('Klaipeda University Hospital')),
	(select person_id from institution_data.person where email =  'petraitis@gmail.com'),
	(select staff_id from institution_data.medical_dtaff where license_number =  '1123497')
where not exists (select * from institution_data.appointment
					where patient_id = (select person_id from institution_data.person where email =  'petraitis@gmail.com')
					and doctor_id = (select staff_id from institution_data.medical_dtaff where license_number =  '1123497'))
union
select '2022-01-29'::date,
	(select health_inst_id from institution_data.health_institution where upper(health_inst_name) = upper('Klaipeda University Hospital')),
	(select person_id from institution_data.person where email =  'salverchuk@gmail.com'),
	(select staff_id from institution_data.medical_dtaff where license_number =  '2312367')
where not exists (select * from institution_data.appointment
					where patient_id = (select person_id from institution_data.person where email =  'salverchuk@gmail.com')
					and doctor_id = (select staff_id from institution_data.medical_dtaff where license_number =  '2312367'))
union
select '2022-02-01'::date,
	(select health_inst_id from institution_data.health_institution where upper(health_inst_name) = upper('Klaipeda University Hospital')),
	(select person_id from institution_data.person where email =  'jonaitiene@gmail.com'),
	(select staff_id from institution_data.medical_dtaff where license_number =  '2312367')
where not exists (select * from institution_data.appointment
					where patient_id = (select person_id from institution_data.person where email =  'jonaitiene@gmail.com')
					and doctor_id = (select staff_id from institution_data.medical_dtaff where license_number =  '2312367'))
union
	select '2022-02-03'::date,
	(select health_inst_id from institution_data.health_institution where upper(health_inst_name) = upper('Klaipeda University Hospital')),
	(select person_id from institution_data.person where email =  'lidija@gmail.com'),
	(select staff_id from institution_data.medical_dtaff where license_number =  '2312367')
where not exists (select * from institution_data.appointment
					where patient_id = (select person_id from institution_data.person where email =  'lidija@gmail.com')
					and doctor_id = (select staff_id from institution_data.medical_dtaff where license_number =  '2312367'))
union
select '2022-02-04'::date,
	(select health_inst_id from institution_data.health_institution where upper(health_inst_name) = upper('Klaipeda University Hospital')),
	(select person_id from institution_data.person where email =  'jonaitiene@gmail.com'),
	(select staff_id from institution_data.medical_dtaff where license_number =  '2312367')
where not exists (select * from institution_data.appointment
					where patient_id = (select person_id from institution_data.person where email =  'jonaitiene@gmail.com')
					and doctor_id = (select staff_id from institution_data.medical_dtaff where license_number =  '2312367'))
union
select '2022-02-04'::date,
	(select health_inst_id from institution_data.health_institution where upper(health_inst_name) = upper('Klaipeda University Hospital')),
	(select person_id from institution_data.person where email =  'salverchuk@gmail.com'),
	(select staff_id from institution_data.medical_dtaff where license_number =  '1123497')
where not exists (select * from institution_data.appointment
					where patient_id = (select person_id from institution_data.person where email =  'salverchuk@gmail.com')
					and doctor_id = (select staff_id from institution_data.medical_dtaff where license_number =  '1123497'))
union
select '2022-02-06'::date,
	(select health_inst_id from institution_data.health_institution where upper(health_inst_name) = upper('VNT medical office')),
	(select person_id from institution_data.person where email =  'anton@gmail.com'),
	(select staff_id from institution_data.medical_dtaff where license_number =  '0123400')
where not exists (select * from institution_data.appointment
					where patient_id = (select person_id from institution_data.person where email =  'anton@gmail.com')
					and doctor_id = (select staff_id from institution_data.medical_dtaff where license_number =  '0123400'))
union
select '2022-02-10'::date,
	(select health_inst_id from institution_data.health_institution where upper(health_inst_name) = upper('Republic Klaipeda Hospital')),
	(select person_id from institution_data.person where email =  'salverchuk@gmail.com'),
	(select staff_id from institution_data.medical_dtaff where license_number =  '0012345')
where not exists (select * from institution_data.appointment
					where patient_id = (select person_id from institution_data.person where email =  'salverchuk@gmail.com')
					and doctor_id = (select staff_id from institution_data.medical_dtaff where license_number =  '0012345'))
returning *;


-- Write a query to identify doctors with insufficient workload (less than 5 patients a month for the past few months)

with app_total as (
select md.staff_id, count(*) as appointment_count, md.first_name, md.last_name from medical_dtaff md
inner join appointment a 
on a.doctor_id = md.staff_id
where a.appointment_date between '2022-01-01' and '2022-02-28'
group by md.staff_id
)
select app_total.staff_id , app_total.appointment_count , app_total.first_name , app_total.last_name 
from app_total 
where  appointment_count < 5;