-- Order numbers and total revenue from 2009 until 2012 which order status is finished

select extract(year from s.time_id) as years,
       count(s.prod_id) as products_sold,
       sum(s.amount_sold) as revenue
      from sh.sales s 
group by 1;



------------------------------------------------------------------------------------------------------------
-- Total sales for each category of product during the period of 1999 and 2001

select p.prod_category,
 	sum(case when extract(year from s.time_id) = '1998' then s.amount_sold else 0 end) as sales_1998,
 	sum(case when extract(year from s.time_id) = '1999' then s.amount_sold else 0 end) as sales_1999,
 	sum(case when extract(year from s.time_id) = '2000' then s.amount_sold else 0 end) as sales_2000,
 	sum(case when extract(year from s.time_id) = '2001' then s.amount_sold else 0 end) as sales_2001
from products p
     inner join sales s on s.prod_id = p.prod_id
group by p.prod_category;



------------------------------------------------------------------------------------------------------------
-- The number of customers transactions for each year

select extract(year from s.time_id) as years,
       count(distinct s.cust_id) as number_of_clients
      from sh.sales s 
group by 1;

