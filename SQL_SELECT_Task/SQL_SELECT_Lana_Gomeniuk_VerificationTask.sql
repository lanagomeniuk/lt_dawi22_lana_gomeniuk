-- TASK 1 --
-- Top-3 most selling movie categories of all time and total dvd rental income for each category.
-- Only consider dvd rental customers from the USA.

select c.category_id , c."name" , count(c.category_id) as movies_sold , sum(p.amount) as total_incom
from category c 
inner join film_category fc 
on fc.category_id = c.category_id 
inner join film f 
on f.film_id = fc.film_id 
inner join inventory i 
on i.film_id = f.film_id 
inner join rental r 
on r.inventory_id = i.inventory_id 
inner join payment p 
on p.rental_id = r.rental_id 
inner join customer c2 
on c2.customer_id = p.customer_id 
inner join address a 
on a.address_id = c2.address_id 
inner join city c3 
on c3.city_id = a.city_id 
inner join country c4 
on c3.country_id  = c4.country_id 
	where c4.country_id  = (select country_id from 
		country
		where upper(country) = 'UNITED STATES')
group by c.category_id, c."name" 
order by movies_sold desc 
limit 3;


-- TASK 2 --
-- For each client, display a list of horrors that he had ever rented (in one column, separated by commas),
-- and the amount of money that he paid for it

select c.customer_id , concat(c.first_name, ' ', c.last_name) as client_name, string_agg(distinct f.title, ', ') as list_of_horror_movies, sum(  p.amount) as money_paid  from customer c
inner join payment p 
on c.customer_id = p.customer_id 
inner join rental r 
on r.rental_id  = p.rental_id 
inner join inventory i 
on i.inventory_id = r.inventory_id 
inner join film f 
on f.film_id = i.film_id 
inner join film_category fc 
on fc.film_id = f.film_id 
inner join category c2 
on c2.category_id = fc.category_id 
	where c2.category_id = (
		select category_id from category 
			where upper(name) = 'HORROR')
group by c.customer_id ;

-- there is another query that returns the same result as above one. it is written with CTE.

with horror_movies as (select f.film_id , c."name"   from film f
	inner join film_category fc 
	on fc.film_id = f.film_id 
	inner join category c
	on c.category_id = fc.category_id 
	where upper( c."name" )= 'HORROR'
	group by  f.film_id , c."name")
select cust.customer_id  , concat(cust.first_name, ' ', cust.last_name) as client_name , string_agg( distinct f.title, ', '), sum(p.amount) as sum_paid  from customer cust
	inner join payment p 
	on p.customer_id = cust.customer_id 
	inner join rental r 
	on p.rental_id = r.rental_id  
	inner join inventory i 
	on i.inventory_id = r.inventory_id 
	inner join film f 
	on f.film_id = i.film_id 
	where i.film_id in (select horror_movies.film_id from horror_movies)
	group by cust.customer_id;
