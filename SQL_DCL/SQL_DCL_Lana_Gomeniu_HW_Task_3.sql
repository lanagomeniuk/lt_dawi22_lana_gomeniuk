
-- 3. Read about row-level security (https://www.postgresql.org/docs/12/ddl-rowsecurity.html) and configure it for your database,
-- so that the customer can only access his own data in "rental" and "payment" tables
-- (verify using the personalized role you previously created).

alter table rental enable row level security;
create policy read_rental on rental
using (client_role = current_user);

alter table payment enable row level security;
create policy read_payment on payment 
using (client_role = current_user);
