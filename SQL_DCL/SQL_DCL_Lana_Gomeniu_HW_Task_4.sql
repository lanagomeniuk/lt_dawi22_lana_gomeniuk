-- 4. Prepare answers to the following questions:

-- � How can one restrict access to certain columns of a database table?
	-- it is possible to restric access to a column by following options:
	GRANT SELECT ON table_name (column1, column2) TO user1;
	GO
	DENY SELECT ON table_name (column3) TO user1;
	go
	
	
-- � What is the difference between user identification and user authentication?
	-- Identification is the ability to identify uniquely a user who is running in the system.
	-- Authentication is the ability to prove that a user is genuinely who that person claims to be.

	
-- � What are the recommended authentication protocols for PostgreSQL?
	-- - Trust authentication
	-- - Password authentication

	
-- � What is proxy authentication in PostgreSQL and what is it for?
	-- Proxy authentication is the process of using a middle tier for user authentication
-- Why does it make the previously discussed role-based access control easier to implement?
	-- the previously discussed role-based access control easier to implement because
	-- they don't require some kind of external security infrastructure.