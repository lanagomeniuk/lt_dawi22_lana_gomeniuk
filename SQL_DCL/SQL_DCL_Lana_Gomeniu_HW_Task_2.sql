-- 2. Implement role-based authentication model for dvd_rental database:
-- � Create group roles: DB developer, backend tester (read-only), customer (read-only for film and actor)
-- � Create personalized role for any customer already existing in the dvd_rental database.
	-- Role name must be client_{first_name}_{last_name} (omit curly brackets).
	-- Customer's payment and rental history must not be empty.
-- � Assign proper privileges to each role.
-- � Verify that all roles are working as intended

-- customer (read-only for film and actor)
create role read_film_and_actor;
grant connect on database postgres to read_film_and_actor;
grant select on table film, actor to read_film_and_actor;
create user customer1 with password 'password1';
grant read_film_and_actor to customer1 ;

----------------------------
revoke select on film from backend_tester;
revoke select on actor from customer;
revoke all privileges on database postgres from db_developer; 
drop role db_developer 
-----------------------------

-- backend tester (read-only)
create role read_tables;
grant connect on database postgres to read_tables;
grant select on all tables in schema public to read_tables;
create user backend_tester with password 'password1';
grant read_tables to backend_tester;

-- DB developer 
create role developer;
grant connect on database postgres to developer;
grant all privileges on database postgres to developer ;
create user developer1 with password 'password1';
grant developer to developer1;


-- � Create personalized role for any customer already existing in the dvd_rental database.
	-- Role name must be client_{first_name}_{last_name} (omit curly brackets).
	-- Customer's payment and rental history must not be empty.

create or replace function create_role(in name_input text, in last_name_input text)
returns text
LANGUAGE plpgsql
AS $$
declare
	role_name text;
	client_role text;
begin 
select distinct concat(c.first_name,' ' ,  c.last_name) from customer c 
into role_name 
inner join rental r on r.customer_id = c.customer_id
inner join payment p on p.customer_id = r.customer_id
where upper(c.first_name) = upper(name_input) and upper(c.last_name) = upper(last_name_input);
if not found then
	raise notice 'Customer "%" "%" does not exists', name_input, last_name_input ;
else
	client_role  = concat('client_', role_name);
	create role client_role;
end if;
end;
$$;

select * from create_role('maria', 'miller')

