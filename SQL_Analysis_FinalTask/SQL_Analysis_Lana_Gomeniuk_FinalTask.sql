------------------------ TASK 1 -------------------------------------
-- Build a query to create sales report that shows sales distribution by gender (F / M), marital status and by age (21-30 / 31-40 / 41-50 / 51-60 years).
-- Calculate the total sales based on the marital status of the customers.

select c.cust_gender , lower( c.cust_marital_status),
	   		sum(case when t.calendar_year - c.cust_year_of_birth between 21 and 30 then s.amount_sold else 0 end) as "21-30",
	   		sum(case when t.calendar_year - c.cust_year_of_birth between 31 and 40 then s.amount_sold else 0 end) as "31-40",
	   		sum(case when t.calendar_year - c.cust_year_of_birth between 41 and 50 then s.amount_sold else 0 end) as "41-50",
	   		sum(case when t.calendar_year - c.cust_year_of_birth between 51 and 60 then s.amount_sold else 0 end) as "51-60"
from customers c 
	inner join sales s on s.cust_id = c.cust_id 
	inner join times t on t.time_id = s.time_id 
where t.calendar_year = 2000 and 
	 lower(cust_marital_status) in ('single','married')
group by c.cust_gender , lower( c.cust_marital_status)
union all 
select ' ' as cust_gender, 'Total for married' ascust_marital_status,
			sum(case when t.calendar_year - c.cust_year_of_birth between 21 and 30 then s.amount_sold else 0 end) as "21-30",
	   		sum(case when t.calendar_year - c.cust_year_of_birth between 31 and 40 then s.amount_sold else 0 end) as "31-40",
	   		sum(case when t.calendar_year - c.cust_year_of_birth between 41 and 50 then s.amount_sold else 0 end) as "41-50",
	   		sum(case when t.calendar_year - c.cust_year_of_birth between 51 and 60 then s.amount_sold else 0 end) as "51-60"
from customers c 
	inner join sales s on s.cust_id = c.cust_id 
	inner join times t on t.time_id = s.time_id 
where t.calendar_year = 2000 and 
	 lower(cust_marital_status) = 'married'
union all 
select ' ' as cust_gender, 'Total for single' as cust_marital_status,
			sum(case when t.calendar_year - c.cust_year_of_birth between 21 and 30 then s.amount_sold else 0 end) as "21-30",
	   		sum(case when t.calendar_year - c.cust_year_of_birth between 31 and 40 then s.amount_sold else 0 end) as "31-40",
	   		sum(case when t.calendar_year - c.cust_year_of_birth between 41 and 50 then s.amount_sold else 0 end) as "41-50",
	   		sum(case when t.calendar_year - c.cust_year_of_birth between 51 and 60 then s.amount_sold else 0 end) as "51-60"
from customers c 
	inner join sales s on s.cust_id = c.cust_id 
	inner join times t on t.time_id = s.time_id 
where t.calendar_year = 2000 and 
	 lower(cust_marital_status) = 'single';
	 
	
------------------------ TASK 2 -------------------------------------
	
-- Create an annual sales report broken down by calendar years and months. Annual sales for January of each year are presented in monetary units ($),
-- the remaining months should contain the dynamics of sales in percent relative to January
	
select * from crosstab(
	'select calendar_year, calendar_month_number,
	case calendar_month_number
			when 1 then january
			else round((sales/january * 100)-100, 2) end as diff
	from
	(
		select t.calendar_year, t.calendar_month_number, sum(s.amount_sold) as sales,
					first_value(sum(s.amount_sold)) over (partition by  t.calendar_year order by t.calendar_month_number) as january
		from sales s 
			inner join times t on t.time_id = s.time_id 
		group by t.calendar_year, t.calendar_month_number
	) tab',
	'select m from generate_series(1,12) m'
)
as ct(calendar_year integer, 
		"Jan $" numeric, "Feb $" numeric, 
		"Mar $" numeric, "Apr $" numeric, "May $" numeric, 
		"Jun $" numeric, "Jul $" numeric, "Aug $" numeric,
		"Sep $" numeric, "Oct $" numeric, "nov $" numeric,
		"Dec $" numeric);
		
	
	
------------------------ TASK 3 -------------------------------------
-- Which products were the most expensive (MAX (Costs.Unit_Price)) in their product category each year (1998-2001)?
	
select prod_name from 
(
	select t.calendar_year, p.prod_category, p.prod_name, c.unit_price,
			max(c.unit_price) over (partition by t.calendar_year, p.prod_category) as max_price
	from products p 
		inner join costs c on c.prod_id = p.prod_id 
		inner join times t on t.time_id = c.time_id
	group by  t.calendar_year , p.prod_category,  p.prod_name, c.unit_price
	order by t.calendar_year, p.prod_category, p.prod_name, c.unit_price
) as tab 
where unit_price = max_price
group by prod_name
having count(*) = 4;	