create database gas_station;

create schema gs_data;

---- city tabble created

create table if not exists gs_data.city (
	city_id serial primary key,
	city_name text not null
);


insert into gs_data.city (city_name)
select 'Gargzdai' where not exists (select * from gs_data.city where upper(city_name) = upper('Gargzdai'))
union 
select 'Palanga' where not exists (select * from gs_data.city where upper(city_name) = upper('Palanga'))
union
select 'Klaipeda' where not exists (select * from gs_data.city where upper(city_name) = upper('Klaipeda'))
union
select 'Kaunas' where not exists (select * from gs_data.city where upper(city_name) = upper('Kaunas'))
union 
select 'Vilnius' where not exists (select * from gs_data.city where upper(city_name) = upper('Vilnius'))
returning *;


-- -- customer table created 

create table if not exists gs_data.customer (
	customer_id serial primary key,
	first_name text not null,
	last_name text not null,
	phone_number text not null,
	email text not null,
	adress text not null,
	city_id integer references gs_data.city,
	constraint chk_phone check (phone_number not like '%[^0-9]%'), -- phone number constraint checks is not a digit 
	constraint chk_email check (email like '%_@__%.__%') -- checks if there are '@' and '.'
);

alter table gs_data.customer alter email set default 'default@gmail.com';
alter table gs_data.customer alter first_name set default 'Vardas';
alter table gs_data.customer alter last_name set default 'Paverdenis';
alter table gs_data.customer alter adress set default 'unknown';
alter table gs_data.customer alter city_id set default null;


with get_klp as (
SELECT city_id FROM gs_data.city
where upper(city_name) = upper('Klaipeda')
)
insert into  gs_data.customer (
	first_name, last_name,
	phone_number, email, adress, city_id
	)
select 'Anna', 'Jonaitiene', '867500006', 'jonaitiene@gmail.com', 'Laukininku g. 7-14', get_klp.city_id
from get_klp
	where not exists (select * from gs_data.customer where email =  'jonaitiene@gmail.com')
union 
select 'Karolis', 'Petraitis','867512456', 'petraitis@gmail.com', 'Mogiliovo g. 37-20', get_klp.city_id
from get_klp
	where not exists (select * from gs_data.customer where email =  'petraitis@gmail.com')
union
select 'Lilija', 'Salverchuk', '860000001', 'salverchuk@gmail.com', 'Taikos pr. 144-28', get_klp.city_id
from get_klp 
	where not exists (select * from gs_data.customer where email =  'salverchuk@gmail.com')
union
select 'Sergej', 'Anochin', '861100045', 'anochin@gmail.com', 'Taikos pr. 103-32', get_klp.city_id
from get_klp
	where not exists (select * from gs_data.customer where email =  'anochin@gmail.com')
union
select 'Lidija', 'Anochina', '861100229', 'lidija@gmail.com', 'Taikos pr. 103-32', get_klp.city_id
from get_klp 
	where not exists (select * from gs_data.customer where email =  'lidija@gmail.com')
	union
select 'Anton', 'Anochin', '860731635', 'anton@gmail.com', 'Taikos pr. 144-27', get_klp.city_id
from get_klp
	where not exists (select * from gs_data.customer where email =  'anton@gmail.com')
returning *;


-- -- tank table created
create table if not exists gs_data.tank (
	tank_id serial primary key,
	tank_serial_no varchar(10),
	tank_capacity integer not null,
	constraint chk_number check (tank_serial_no not like '%[^0-9]%')
);

insert into gs_data.tank (tank_serial_no, tank_capacity)
select '00012345', 50000
	where not exists (select * from gs_data.tank where tank_serial_no = '00012345')
union 
select '00012346', 50000
	where not exists (select * from gs_data.tank where tank_serial_no = '00012346')
union 
select '00012350', 50000
	where not exists (select * from gs_data.tank where tank_serial_no = '00012350')
union 
select '00012355', 50000
	where not exists (select * from gs_data.tank where tank_serial_no = '00012355')
returning *;



-- -- gas table created -- --
create table if not exists gs_data.gas (
	gas_id serial primary key,
	gas_type text not null,
	gas_price numeric(4, 2) not null,
	tank_id integer references gs_data.tank
);


insert into gs_data.gas (gas_type, gas_price, tank_id)
select '95', 1.79, (select tank_id from gs_data.tank where tank_serial_no = '00012345')
	where not exists (select * from gs_data.gas where gas_type = '95')
union 
select '98', 1.85, (select tank_id from gs_data.tank where tank_serial_no = '00012346')
	where not exists (select * from gs_data.gas where gas_type = '98')
union 
select 'diesel', 1.69, (select tank_id from gs_data.tank where tank_serial_no = '00012350')
	where not exists (select * from gs_data.gas where upper(gas_type) = upper('diesel'))
union 
select 'gas', 1.69, (select tank_id from gs_data.tank where tank_serial_no = '00012355')
	where not exists (select * from gs_data.gas where upper(gas_type) = upper('gas'))
returning *;


-- -- gas table created -- --
create table if not exists gs_data.staff (
	staff_id serial primary key,
	first_name text not null,
	last_name text not null,
	phone_number text not null,
	email text not null,
	constraint chk_phone_staff check (phone_number not like '%[^0-9]%'),
	constraint chk_email_staff check (email like '%_@__%.__%')
);

insert into  gs_data.staff (
	first_name, last_name,
	phone_number, email
	)
select 'Olegas', 'Kovaliovas', '867500161', 'kovaliovas@gmail.com'
	where not exists (select * from gs_data.staff where email =  'kovaliovas@gmail.com')
union 
select 'Karolis', 'Kvederas','867511234', 'kvederas@gmail.com'
	where not exists (select * from gs_data.staff where email =  'kvederas@gmail.com')
union
select 'Lilija', 'Petrova', '860002201', 'petrova@gmail.com'
	where not exists (select * from gs_data.staff where email =  'petrova@gmail.com')
union
select 'Sergej', 'Kolokolcev', '861104321', 'kolokolcev@gmail.com'
	where not exists (select * from gs_data.staff where email =  'kolokolcev@gmail.com')
union
select 'Lidija', 'Guceliuk', '861544229', 'guceliuk@gmail.com'
	where not exists (select * from gs_data.staff where email =  'guceliuk@gmail.com')
	union
select 'Jonas', 'Martynovas', '860731677', 'martynovas@gmail.com'
	where not exists (select * from gs_data.staff where email =  'martynovas@gmail.com')
returning *;


-- -- gas_station table created -- --
create table if not exists gs_data.gas_station (
	gas_st_id serial primary key,
	gas_st_title text not null default 'NESTE',
	gas_st_adress text not null,
	city_id integer references gs_data.city,
	staff_id integer references gs_data.staff 
);


with get_klp as (
select city_id FROM gs_data.city
where upper(city_name) = upper('Klaipeda')
),
get_vln as (
select city_id FROM gs_data.city
where upper(city_name) = upper('Vilnius')
),
get_kns as (
select city_id FROM gs_data.city
where upper(city_name) = upper('Kaunas')
)
insert into gs_data.gas_station (gas_st_title, gas_st_adress, city_id, staff_id)
select 'Neste Taikos', 'Taikos pr. 60', get_klp.city_id,
	(select staff_id from gs_data.staff where upper(email) = upper('kovaliovas@gmail.com'))
from get_klp 
	where not exists (select * from gs_data.gas_station where upper(gas_st_title) = upper('Neste Taikos'))
union 
select 'Neste Minijos', 'Minijos g. 119', get_klp.city_id,
	(select staff_id from gs_data.staff where upper(email) = upper('kvederas@gmail.com'))
from get_klp
	where not exists (select * from gs_data.gas_station where upper(gas_st_title) = upper('Neste Minijos'))
union
select 'Neste Litexpo', 'Parodu g. 1A', get_vln.city_id,
	(select staff_id from gs_data.staff where upper(email) = upper('kolokolcev@gmail.com'))
from get_vln
	where not exists (select * from gs_data.gas_station where upper(gas_st_title) = upper('Neste Litexpo'))
union
select 'Neste Rimi', 'Kedru g. 2', get_vln.city_id,
	(select staff_id from gs_data.staff where upper(email) = upper('martynovas@gmail.com'))
from get_vln 
	where not exists (select * from gs_data.gas_station where upper(gas_st_title) = upper('Neste Rimi'))
returning *;

-- -- gas_pump table created -- --
create table if not exists gs_data.gas_pump (
	gas_pump_id serial primary key,
	serial_nr text not null,
	gas_id integer references gs_data.gas,
	gas_st_id integer references gs_data.gas_station,
	constraint chk_serila_nr check (serial_nr not like '%[^0-9]%')
);

with get_95 as (
select gas_id from gs_data.gas
where upper(gas_type) = upper('95')
),
get_98 as (
select gas_id from gs_data.gas
where upper(gas_type) = upper('98')
),
get_gas as (
select gas_id from gs_data.gas
where upper(gas_type) = upper('gas')
),
get_diesel as (
select gas_id from gs_data.gas
where upper(gas_type) = upper('diesel')
)
insert into gs_data.gas_pump (gas_id, serial_nr, gas_st_id)
select get_95.gas_id, '120001', (select gas_st_id from gs_data.gas_station where upper(gas_st_title) = upper('Neste Taikos'))
from get_95 
	where not exists (select * from gs_data.gas_pump where serial_nr = '120001')
union 
select get_98.gas_id, '120002', (select gas_st_id from gs_data.gas_station where upper(gas_st_title) = upper('Neste Taikos'))
from get_98 
	where not exists (select * from gs_data.gas_pump where serial_nr = '120002')
union
select get_95.gas_id, '120003', (select gas_st_id from gs_data.gas_station where upper(gas_st_title) = upper('Neste Taikos'))
from get_95 
	where not exists (select * from gs_data.gas_pump where serial_nr = '120003')
union
select get_diesel.gas_id, '120004',
		(select gas_st_id from gs_data.gas_station where upper(gas_st_title) = upper('Neste Taikos'))
from get_diesel
	where not exists (select * from gs_data.gas_pump where serial_nr = '120004')
union
select get_98.gas_id, '120005', (select gas_st_id from gs_data.gas_station where upper(gas_st_title) = upper('Neste Taikos'))
from get_98 
	where not exists (select * from gs_data.gas_pump where serial_nr = '120005')
returning *;



select '2022-03-01 14:25:35'::timestamp , 44.50,
	(select gas_pump_id from gas_pump where serial_nr  = '120002')
	where not exists (select * from gs_data.fuelling where fuelling_date = '2022-03-01  14:25:35' and amount = 44.50)
union
select '2022-03-01 14:45:35'::timestamp , 41.99,
	(select gas_pump_id from gas_pump where serial_nr  = '120002')
	where not exists (select * from gs_data.fuelling where fuelling_date = '2022-03-01  14:45:35' and amount = 41.99)

-- -- payment table -- --
create table if not exists gs_data.payment (
	payment_id serial primary key,
	customer_id integer references gs_data.customer,
	gas_pump_id integer references gs_data.gas_pump,
	amount decimal(4, 2) not null,
	total_sum decimal(4, 2) not null,
	payment_date timestamp not null default now()
);

drop table gs_data.payment cascade

insert into gs_data.payment (customer_id, gas_pump_id, amount, total_sum, payment_date)
select (select customer_id from gs_data.customer where upper(email) = upper('jonaitiene@gmail.com')),
	(select gas_pump_id from gas_pump where serial_nr  = '120004'), 30.50, 
	(30.50 * (select gas_price from gs_data.gas where gas_id = (select gas_id from gas_pump where serial_nr  = '120004'))),
	'2022-03-01 12:21:31'::timestamp
where not exists (select * from gs_data.payment
		where customer_id = (select customer_id from gs_data.customer where upper(email) = upper('jonaitiene@gmail.com'))
		and payment_date = '2022-03-01 12:21:31'::timestamp)
union 
select (select customer_id from gs_data.customer where upper(email) = upper('petraitis@gmail.com')),
	(select gas_pump_id from gas_pump where serial_nr  = '120003'), 10.35, 
	(10.35 * (select gas_price from gs_data.gas where gas_id = (select gas_id from gas_pump where serial_nr  = '120003'))),
	'2022-03-01 13:23:33'::timestamp
where not exists (select * from gs_data.payment
		where customer_id = (select customer_id from gs_data.customer where upper(email) = upper('petraitis@gmail.com'))
		and payment_date = '2022-03-01 13:23:33'::timestamp)
union 
select (select customer_id from gs_data.customer where upper(email) = upper('anton@gmail.com')),
	(select gas_pump_id from gas_pump where serial_nr  = '120005'), 50.55, 
	(50.55 * (select gas_price from gs_data.gas where gas_id = (select gas_id from gas_pump where serial_nr  = '120005'))),
	'2022-03-01 14:24:34'::timestamp
where not exists (select * from gs_data.payment
		where customer_id = (select customer_id from gs_data.customer where upper(email) = upper('anton@gmail.com'))
		and payment_date = '2022-03-01 14:24:34'::timestamp)
union 
select (select customer_id from gs_data.customer where upper(email) = upper('lidija@gmail.com')),
	(select gas_pump_id from gas_pump where serial_nr  = '120001'), 14.67, 
	(14.67 * (select gas_price from gs_data.gas where gas_id = (select gas_id from gas_pump where serial_nr  = '120001'))),
	'2022-03-01 14:25:35'::timestamp
where not exists (select * from gs_data.payment
		where customer_id = (select customer_id from gs_data.customer where upper(email) = upper('lidija@gmail.com'))
		and payment_date = '2022-03-01 14:25:35'::timestamp)
union 
select (select customer_id from gs_data.customer where upper(email) = upper('salverchuk@gmail.com')),
	(select gas_pump_id from gas_pump where serial_nr  = '120002'), 44.59, 
	(44.59 * (select gas_price from gs_data.gas where gas_id = (select gas_id from gas_pump where serial_nr  = '120002'))),
	'2022-03-01 19:25:35'::timestamp
where not exists (select * from gs_data.payment
		where customer_id = (select customer_id from gs_data.customer where upper(email) = upper('salverchuk@gmail.com'))
		and payment_date = '2022-03-01 19:25:35'::timestamp)
returning *;


------------------------------------------------------------------------------------------------------------------------------
--4. Create the following functions:
-- � Function that UPDATEs data in one of your tables (input arguments: table's primary key value, column name and column value to UPDATE to).

create or replace function update_customer(input_client_id integer, input_column text, input_value text)
returns table (customer_id_o integer , first_name_o text, last_name_o text, phone_number_o text, email_o text, adress_o text, city_id_o integer) 
language plpgsql 
as $$
declare 
client_id integer ;
begin 
-- lookup for customer_id
	select customer_id into client_id 
	from customer c 
	where customer_id = input_client_id;
if not found then
	     raise notice 'Customer whith id "%" does not exists', input_client_id;
else 
	execute format('update gs_data.customer set %I = $1
			where customer_id = $2', input_column)
			using   input_value, input_client_id ;
			return query (select * from customer where customer_id = input_client_id);
end if;
end;
$$

select * from update_customer(2, 'email', 'anton007@gmail.com');


----------------------------------------------------------------------------------------------------------------
-- � Function that adds new transaction to your transaction table. Come up with input arguments and output format yourself.

drop function add_payment(text, integer, decimal)
select  *from payment p 

create or replace function add_payment(input_phone text, input_pump_id integer, input_amount decimal(4, 2))
returns table (out_id int, out_customer_id integer, out_pump_id integer, out_amount decimal, out_total decimal, out_pay_date timestamp)
language plpgsql
as $$
declare 

cust_id integer;
price decimal(4, 2);

begin 
--- lookup for pump_id ----
select gas_pump_id into pump_id 
from gs_data.gas_pump 
	where gas_pump_id  = input_pump_id ;

---- lookup for gas_price -------
	price = (select g.gas_price  from gas g
			inner join gas_pump gp on gp.gas_id = g.gas_id
			where gp.gas_pump_id  = input_pump_id 
			);
---- lookup for customer_id -------
		select c.customer_id into cust_id 
		from customer c
		where c.phone_number = input_phone;
	
if not found then
	     raise notice 'gas_pump_id "%" does not exists', input_pump_id ;
else
---- checks if customer doesn't exist and creates new custumer. after that inserts new payment record.
if input_phone not in (select c.phone_number from customer c) 
and input_pump_id in (select gp.gas_pump_id from gas_pump gp)
	then 
---- insert new customer  -------	
		insert into gs_data.customer(phone_number)
		select (input_phone);
	
---- lookup for customer_id -------
		select c.customer_id into cust_id 
		from customer c
		where c.phone_number = input_phone;
		
---- insert new payment  -------		
		insert into gs_data.payment (customer_id, gas_pump_id, amount, total_sum)
		select cust_id, input_pump_id, input_amount, (input_amount * price)
		where not exists (select * from gs_data.payment where gas_pump_id  = input_pump_id and amount = input_amount
							and amount = (input_amount * price));

else -- if customer and gas pump exists in the database , new payment record is added.
			insert into gs_data.payment (customer_id, gas_pump_id, amount, total_sum)
			select cust_id, input_pump_id, input_amount, (input_amount * price)
			where not exists (select * from gs_data.payment where gas_pump_id  = input_pump_id and amount = input_amount
							and amount = (input_amount * price));

return query (select * from gs_data.payment where customer_id  = cust_id and amount = input_amount);
	end if;	
end if;
end;
$$

select * from add_payment('867512455', 1, 33.33);


----------------------------------------------------------------------------------------------------------------------
-- 5. Create view that joins all tables in your database and represents data in denormalized form for the past month.

create or replace view gs_data.customer_payment_info
as select p.payment_date, concat(c.first_name, ' ', c.last_name) as full_name, gs.gas_st_id, p.amount, p.total_sum from customer c 
inner join payment p on p.customer_id = c.customer_id 
inner join gas_pump gp on gp.gas_pump_id = p.gas_pump_id 
inner join gas g on g.gas_id  = gp.gas_id 
inner join gas_station gs on gp.gas_st_id = gs.gas_st_id;

--6. Create manager's read-only role. Make sure he can only SELECT from tables in your database. 

DO
$$
BEGIN
   IF NOT EXISTS (
      SELECT FROM pg_catalog.pg_roles WHERE  rolname = 'manager') THEN
      CREATE ROLE manager LOGIN PASSWORD 'password1';
     GRANT SELECT ON ALL TABLES in schema gs_data to manager;
   END IF;
END
$$;