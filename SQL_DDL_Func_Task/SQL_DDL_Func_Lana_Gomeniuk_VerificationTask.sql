
create or replace function get_client_info (in input_id integer, in input_start date, in input_end date)
returns table (metric_name text, metric_value text)
language plpgsql
as $$
begin
return query
select 'customer''s info' as matric_name, (c.first_name || ' '::text) || (c.last_name || ', '::text) || (c.email || ' '::text) as metric_value 
from customer c
	where c.customer_id = input_id
union all
select 'num. of films rented' as matric_name, count(r.customer_id)::text metric_value 
from rental r  
	where r.customer_id = input_id
	and r.rental_date between input_start  and input_end 
union all 
select 'rented films'' titles' as matric_name, group_concat(f.title || ' '::text) as metric_value
from film f 
	inner join inventory i on i.film_id = f.film_id 
	inner join rental r2 on r2.inventory_id = i.inventory_id 
	inner join customer c2 on c2.customer_id = r2.customer_id
	where c2.customer_id = input_id
	and r2.rental_date  between input_start  and input_end 
union all 
select 'num. of payments' as matric_name, count(p.customer_id)::text metric_value 
from payment p 
	where p.customer_id = input_id
	and p.payment_date between input_start  and input_end 
union all 
select 'payments'' amount ' as matric_name, sum(p2.amount)::text metric_value
from payment p2 
	where p2.customer_id = input_id 
	and p2.payment_date between input_start  and input_end;
end;
$$

select * from get_client_info(55, '2005-01-01'::date, '2017-12-31'::date);