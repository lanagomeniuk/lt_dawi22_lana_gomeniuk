-- Build the query to generate a report about the most significant customers (which have maximum sales) through various sales channels. 
-- The 5 largest customers are required for each channel.
-- Column sales_percentage shows percentage of customerís sales within channel sales 

select  channel_desc, cust_last_name, cust_first_name, amount_sold, sales_percentage
from (
	select  c.channel_desc, c2.cust_last_name, c2.cust_first_name,
			sum(s.amount_sold) as amount_sold,
			concat(round(cume_dist () over (partition by c.channel_desc order by  sum(s.amount_sold) desc)::numeric * 100, 5), ' %') as sales_percentage,
			row_number() over (partition by c.channel_desc order by sum(s.amount_sold)  desc) as rank_in_line 
	from channels c 
	inner join sales s on s.channel_id = c.channel_id 
	inner join customers c2 on c2.cust_id = s.cust_id
	group by c.channel_desc, c.channel_id, c2.cust_id 
	) as tab
where rank_in_line <=5
order by amount_sold desc;


-----------------------------------------------------------------------------------------------------------------------
-- Compose query to retrieve data for report with sales totals for all products in Photo category in Asia (use data for 2000 year).
-- Calculate report total (YEAR_SUM).

CREATE EXTENSION IF NOT EXISTS tablefunc;

with res_tabl as (
	select prod_name, coalesce("q1" , 0) as "q1", coalesce("q2" , 0) as "q2", coalesce("q3" , 0) as "q3", coalesce("q4" , 0) as "q4" from crosstab (
		'select  p.prod_name, t.calendar_quarter_number as q, sum(s.amount_sold) 
		from products p 
		inner join sales s on s.prod_id = p.prod_id 
		inner join customers c on c.cust_id = s.cust_id 
		inner join countries c2 on c2.country_id = c.country_id
		inner join times t on t.time_id  = s.time_id 
		where p.prod_category  = ''Photo'' and c2.country_subregion = ''Asia'' and t.calendar_year = 2000
		group by p.prod_name, t.calendar_quarter_number',
		'select q from generate_series(1,4) q'
)
as ct(prod_name text , "q1" numeric, "q2" numeric, "q3" numeric, "q4" numeric))
select prod_name , q1, q2, q3, q4, (q1+ q2 + q3 + q4) as sum_year
from res_tabl ;

--------------------------------------------------------------------------------------------------
-- Build the query to generate a report about customers who were included into TOP 300 (based on the amount of sales) in 1998, 1999 and 2001.
-- This report should separate clients by sales channels, and, at the same time, channels should be calculated independently.

with client_tab as (	-- select customers from 1998 to 2000
	select  c.channel_desc, c2.cust_id, c2.cust_last_name, c2.cust_first_name,
			sum(s.amount_sold) as amount_sold, extract  (year from s.time_id),
			row_number() over (partition by c.channel_id, extract  (year from s.time_id) order by sum(s.amount_sold) desc) as row_num
	from channels c 
		inner join sales s on s.channel_id = c.channel_id 
		inner join customers c2 on c2.cust_id = s.cust_id
	where extract (year from s.time_id) in (1998, 1999, 2001)
	group by extract (year from s.time_id), c.channel_id, c.channel_desc, c2.cust_id, c2.cust_last_name, c2.cust_first_name
)
select channel_desc, cust_id, cust_last_name, cust_first_name, sum(amount_sold) as amount_sold
from client_tab
	where row_num <=300
group by channel_desc, cust_id, cust_last_name, cust_first_name
	having count(*) = 3 
order by amount_sold desc, cust_id asc;


---------------------------------------------------------------------------------------------------------------------
-- Build the query to generate the report about sales in America and Europe

select  t.calendar_month_desc, p.prod_category, 
		sum(case when c2.country_region = 'Americas' then s.amount_sold else 0 end) as Americas_SALES,
		sum(case when c2.country_region = 'Europe' then s.amount_sold else 0 end) as Europe_SALES
from products p 
	inner join sales s on s.prod_id = p.prod_id 
	inner join customers c on c.cust_id = s.cust_id 
	inner join countries c2 on c2.country_id = c.country_id
	inner join times t on t.time_id  = s.time_id 
where t.calendar_month_desc in ('2000-01', '2000-02', '2000-03')
group by t.calendar_month_desc, p.prod_category;
