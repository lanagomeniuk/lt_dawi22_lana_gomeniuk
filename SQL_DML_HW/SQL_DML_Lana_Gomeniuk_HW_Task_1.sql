-- task 1
--Choose your top-3 favorite movies and add them to 'film' table.
--Fill rental rates with 4.99, 9.99 and 19.99 and rental durations with 1, 2 and 3 weeks respectively.

insert into film (title, rental_rate, rental_duration, language_id)
select 
	'THE GREEN MILE', 4.99, 1, language_id
from "language" where "name" = 'English';

insert into film (title, rental_rate, rental_duration, language_id)
select 
	'FORREST GUMP', 9.99, 2, language_id
from "language" where "name" = 'English';

insert into film (title, rental_rate, rental_duration, language_id)
select 
	'CAPTAIN PHILLIPS', 19.99, 3, language_id
from "language" where "name" = 'English';


-- task 2
-- Add actors who play leading roles in your favorite movies to 'actor' and 'film_actor' tables (6 or more actors in total).

with table_actor as (
insert into actor (first_name, last_name)
select 'TOM' as first_name , 'HANKS' as last_name
where not exists (select * from actor a where a.first_name = 'TOM' and a.last_name =  'HANKS')
returning actor_id
)
insert into film_actor(actor_id, film_id)
select
(select actor_id  from table_actor),
(select film_id from film f 
where f.title = 'THE GREEN MILE');

--------------
with table_actor2 as (
insert into actor (first_name, last_name)
select 'MICHAEL' as first_name , 'DUNCAN' as last_name
where not exists (select * from actor a where a.first_name = 'MICHAEL' and a.last_name =  'DUNCAN')
returning actor_id
)
insert into film_actor(actor_id, film_id)
select
(select actor_id  from table_actor2),
(select film_id from film f 
where f.title = 'THE GREEN MILE');

-------------
with table_actor2 as (
insert into actor (first_name, last_name)
select 'DAVID' as first_name , 'MORSE' as last_name
where not exists (select * from actor a where a.first_name = 'DAVID' and a.last_name =  'MORSE')
returning actor_id
)
insert into film_actor(actor_id, film_id)
select
(select actor_id  from table_actor2),
(select film_id from film f 
where f.title = 'THE GREEN MILE');

-----------
with table_actor as (
insert into actor (first_name, last_name)
select 'GARY' as first_name , 'SINISE' as last_name
where not exists (select * from actor a where a.first_name = 'GARY' and a.last_name =  'SINISE')
returning actor_id
)
insert into film_actor(actor_id, film_id)
select
(select actor_id  from table_actor),
(select film_id from film f 
where f.title = 'FORREST GUMP');

--------------------
with table_actor as (
insert into actor (first_name, last_name)
select 'ROBIN' as first_name , 'WRIGHT' as last_name
where not exists (select * from actor a where a.first_name = 'ROBIN' and a.last_name =  'WRIGHT')
returning actor_id
)
insert into film_actor(actor_id, film_id)
select
(select actor_id  from table_actor),
(select film_id from film f 
where f.title = 'FORREST GUMP');

--------------------
insert into film_actor(actor_id, film_id)
select
(select actor_id  from actor where first_name = 'TOM' and last_name = 'HANKS'),
(select film_id from film f 
where f.title = 'FORREST GUMP');

--------------------
insert into film_actor(actor_id, film_id)
select
(select actor_id  from actor where first_name = 'TOM' and last_name = 'HANKS'),
(select film_id from film f 
where f.title = 'THE GREEN MILE');



-- task 3
-- Add your favorite movies to any store's inventory

insert into inventory (film_id, store_id)
select film_id , 2
from film f
where f.title = 'CAPTAIN PHILLIPS';

insert into inventory (film_id, store_id)
select film_id , 2
from film f
where f.title = 'FORREST GUMP';

insert into inventory (film_id, store_id)
select film_id , 2
from film f
where f.title = 'THE GREEN MILE';


-- task 4
-- Alter any existing customer in the database who has at least 43 rental and 43 payment records.
-- Change his/her personal data to yours (first name, last name, address, etc.).
-- Do not perform any updates on 'address' table, as it can impact multiple records with the same address.
-- Change customer's create_date value to current_date.

select c.customer_id  from customer c 
inner join payment p 
on p.customer_id = c.customer_id
group by  c.customer_id
having count(p.rental_id) >= 43 and  count(p.payment_id) >= 43;

-- I am going to update customer with customer_id 184
update customer 
set first_name = 'LANA', last_name = 'GOMENIUK', email = 'ANOCHINA@hotmail.com', create_date = current_date 
where customer_id = 184;


-- task 5
-- Remove any records related to you (as a customer) from all tables except 'Customer' and 'Inventory'

delete from payment  -- I deleted all record thata are related to customer with customer_id = 184 from payment table
where customer_id = 184;

delete from rental -- -- I deleted all record thata are related to customer with customer_id = 184 from inventory table
where customer_id = 184;



-- task 6
-- Rent you favorite movies from the store they are in and pay for them (add corresponding records to the database to represent this activity)

-- I insert values to rental table and paymant in one query by using CTE

with rental_table as 
(
insert into rental (rental_date, inventory_id, customer_id, return_date, staff_id)
select '2017-01-05 13:22:05', inventory_id , 184, '2017-01-10 14:22:05', 2 from inventory i 
	inner join film f 
	on i.inventory_id = f.film_id
	where f.title = 'THE GREEN MILE'
	returning *
)
insert into payment (customer_id, staff_id, rental_id, amount, payment_date)
select 
	184, (select staff_id from rental_table), (select rental_id from rental_table), 4.99, '2017-01-10 14:22:44';


-------------------------------------------
with rental_table as 
(
insert into rental (rental_date, inventory_id, customer_id, return_date, staff_id)
select '2017-01-10 13:22:05', inventory_id , 184,'2017-01-15 15:22:05', 5 from inventory i 
	inner join film f 
	on i.inventory_id = f.film_id
	where f.title = 'FORREST GUMP'
	returning rental_id
)
insert into payment (customer_id, staff_id, rental_id, amount, payment_date)
select 
	184, 5, (select rental_id from rental_table), 9.99, '2017-01-15 15:22:05';


---------------------------------
with rental_table as 
(
insert into rental (rental_date, inventory_id, customer_id, return_date, staff_id)
select '2017-01-15 16:22:05', inventory_id , 184,'2017-01-20 10:10:10', 2 from inventory i 
	inner join film f 
	on i.inventory_id = f.film_id
	where f.title = 'CAPTAIN PHILLIPS'
returning rental_id
)
insert into payment (customer_id, staff_id, rental_id, amount, payment_date)
select 
	184, 2, (select rental_id from rental_table), 19.99, '2017-01-20 10:10:10';
