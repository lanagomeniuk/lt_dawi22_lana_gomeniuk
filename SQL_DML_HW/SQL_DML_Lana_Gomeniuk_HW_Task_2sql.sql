-- 1. Create table �table_to_delete� and fill it with the following query:


CREATE TABLE table_to_delete AS
SELECT 'veeeeeeery_long_string' || x AS col
FROM generate_series(1,(10^7)::int) x; -- generate_series() creates 10^7 rows of sequential numbers from 1 to 10000000 (10^7)


-- 2. Lookup how much space this table consumes with the following query:

-- this table consumes 'total_bytes 602,398,720, 'table_bytes' 602,390,528 and 574 MB of space

SELECT *, pg_size_pretty(total_bytes) AS total, 
pg_size_pretty(index_bytes) AS INDEX,
pg_size_pretty(toast_bytes) AS toast,
pg_size_pretty(table_bytes) AS TABLE
FROM ( SELECT *, total_bytes-index_bytes-COALESCE(toast_bytes,0) AS table_bytes
FROM (SELECT c.oid,nspname AS table_schema, 
relname AS TABLE_NAME,
c.reltuples AS row_estimate,
pg_total_relation_size(c.oid) AS total_bytes,
pg_indexes_size(c.oid) AS index_bytes,
pg_total_relation_size(reltoastrelid) AS toast_bytes
FROM pg_class c
LEFT JOIN pg_namespace n ON n.oid = c.relnamespace
WHERE relkind = 'r'
) a
) a
WHERE table_name LIKE '%table_to_delete%';


-- 3. Issue the following DELETE operation on �table_to_delete�:

DELETE FROM table_to_delete
WHERE REPLACE(col, 'veeeeeeery_long_string','')::int % 3 = 0; -- removes 1/3 of all rows

-- a. Note how much time it takes to perform this DELETE statement;
	-- it took 10.434s to perform 
-- b. Lookup how much space this table consumes after previous DELETE;
	-- after executing query from the task above, I got such results:
	-- 'total_bytes 602,562,560, 'table_bytes' 602,554,368 and 575 MB of space
-- c. Perform the following command (if you're using DBeaver, press Ctrl+Shift+O to observe server output (VACUUM results)):

VACUUM FULL VERBOSE table_to_delete;

-- vacuuming "public.table_to_delete"
-- "table_to_delete": found 0 removable, 6666667 nonremovable row versions in 73530 pages

--d. Check space consumption of the table once again and make conclusions;
-- -- after executing query once again, I got such results:
	-- 'total_bytes 401,629,184, 'table_bytes' 401,620,992 and 383 MB of space
	-- According to the results it is obvious that amount of taken space significantly decreased after performing VACUUM command 

--e. Recreate �table_to_delete� table

drop table table_to_delete;

CREATE TABLE table_to_delete AS
SELECT 'veeeeeeery_long_string' || x AS col
FROM generate_series(1,(10^7)::int) x;


-- 4. Issue the following TRUNCATE operation:

TRUNCATE table_to_delete;

-- a. Note how much time it takes to perform this TRUNCATE statement. 
	-- it took 80ms

-- b. Compare with previous results and make conclusion.
-- previous result was 10.434s. it means that truncate operation took less than 1 sec

-- c. Check space consumption of the table once again and make conclusions;
	-- 'total_bytes' 8,192, 'table_bytes' 0 and 0 bytes of space;
	-- after perfoming truncate the table doesn't take any space .

-- 5. Hand over your investigation's results to your trainer. The results must include:

--a. Space consumption of �table_to_delete� table before and after each operation

	-- 'total_bytes 602,398,720, 'table_bytes' 602,390,528 and 574 MB of space
	-- 'total_bytes 602,562,560, 'table_bytes' 602,554,368 and 575 MB of space
	-- 'total_bytes 8,192, 'table_bytes' 0 and 0 bytes of space;

-- b. Duration of each operation (DELETE, TRUNCATE)

-- DELETE took 10.434s to perform 
-- TRUNCATE took 80ms

